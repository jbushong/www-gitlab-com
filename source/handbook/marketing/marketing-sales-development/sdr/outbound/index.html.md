---
layout: markdown_page
title: "Outbound SDR"
---
---


# Outbound SDR Handbook
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

## Your Role

As an Outbound Sales Development Representative (SDR), you will be responsible for one of the most difficult and important parts of growing a business: outbound prospecting. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities, within our large and strategic account segments.

## SDR Expectations

* Meet monthly quota of Sales Accepted Opportunities
* Exceptional salesforce hygiene, logging all prospecting activity, opportunity creation 
* Maintaining a high sense of autonomy to focus on what's most important; created and sales accepted opportunities
* Attendance in each initial qualifying meeting created - documenting notes and communicating with your SAL after the meeting 
  - It will be in your best interest to sit in on as many meetings as possible at different stages in the buying process to see how the SAL's work with prospects beyond qualifying. The better you understand the SALs you work with, the better you will be at making them successful through outbound prospecting support.

## Working with Account Executives

Strategic Account Executives (SALs) receive support from the Outbound SDR team. Meeting cadence consist of the following:

- **Initial kick-off meeting**: Time - 1 hour; Discuss strategy, accounts, and schedules
- **Weekly Status Meeting**: Time - 30 minutes; Discuss initial meetings, opportunities, and prospecting strategy
- **Monthly Recurring Strategy Meeting**: Time - 1 hour; Evaluate strategy and opportunities

Additional ad-hoc meetings may be scheduled in addition to the above.

Strategic Account Leaders (SALs) have direct impact on the Outbound SDR's success. It is very important to form a strong partnership to as you begin your prospecting efforts. The SALs you work with will know a lot about the accounts you are prospecting into, so be sure to communicate regularly to ensure you are as effective as possible.

## Working with an Account Research Specialist

The role of Account Research Specialist supports SDRs by identifying strategic initiatives within a large or strategic sales segment account, and key contacts within the account, to help the SDR team stay focused on prospecting and improve SDR productivity. SDRs can request support in researching accounts that have been identified in coordination with the Outbound SDR Team Lead and the respective Account Executive.

To submit a request, open an issue in the Marketing Project with the following criteria:
*  "Research Request - {Account Name}" in the title field
*  Research requirements in the description field
*  A link to the account in SFDC in the description field
*  Assignee as yourself and the Account Research Specialist
*  Issue marked as confidential as issues will contain information sourced through paid services

SDRs should be as specific as possible in identifying what information they are looking for. Examples of what a SDR could request, but not limited to, are:

*  An account's organizational structure
*  Enterprise-wide initiatives that can be tied to GitLab
*  Clarity into an account's CE Usage Statistics
*  Insights found in an account's annual report
*  Contacts in account leadership positions and changes in leadership
*  Contacts responsible for particular aspects of the software development lifecycle, such as tools, phases, and methodologies
*  Direct dials on certain contacts
*  Planned speaking engagements for relevant contacts
*  Articles for reference

The Account Research Specialist will follow up to determine a final due date based upon number of tasks at hand and communicate to the SDR when the research has been completed.

The research conducted for SDRs should provide value by identifying information that can be used to get in contact with the right people faster, and account-specific information that can be used to improve the chance of securing an introductory meeting. A strong relationship with the Account Research Specialist, collaboration, and feedback will be essential in delivering useful results.

## Compensation and Quota

SDR’s total compensation is based on two key metrics:
* Sales Accepted Opportunities (SAOs)
* Closed won business from SAOs - 1% commission for any closed won opportunity produced, **so as long as the rep is employed as a SDR by GitLab.

Quota is based solely on SAOs. Think of the 1% commission on opportunities you source that a sales person subsequently closes and wins as an added bonus. To be paid on your closed won business, your activity should clearly depict that your prospecting work sourced the opportunity.

SDRs will be expected to get 2 SAOs within the first 28 days of employment, 3 SAOs by the end of their second month, 5 by month 3, and 6 by month 4. After month 4, SDRs will be considered fully ramped and expected to generate 8 SAOs per month.

There is an accelerator for SDRs who deliver over their SAO quota. The accelerator only applies to SAOs; the 1% commission on closed won business is not eligible. Your commission is calculated as follows:

* Tier 1 - Base rate: Per accepted opportunity commission is paid at a base rate of your monthly variable on target earnings divided by your monthly target.
* Tier 2 - Accelerator from 100% to 200% achievement: Each accepted opportunity after initial target is achieved will be paid at 1.05 times the base rate.
* Tier 3 - Accelerator from 200% achievement onwards: After 200% quota achievement each opportunity after will be paid at 1.1 times the base rate.
* There is no floor
* Payment tiers are progressive.

Below are reports from Salesforce that GitLab leadership uses to see all the Outbound created and sales accepted opportunities. If you believe an opportunity you sourced is not reflected in the reporting, notify your manager. 

* [Outbound SDR created opportunities](https://na34.salesforce.com/00O61000003nmhe)
* [Outbound SDR sales accepted opportunities](https://na34.salesforce.com/00O61000003nmhU?dbw=1)

**SDRs will also be measured on the following:**

* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent

The above measurements do not impact your quota attainment, but are critical to being a successful SDR. A SDRs main focus should always be achieving their SAO quota.

## How to SDR

The key to successful prospecting begins with spending time in the right accounts, doing research, and developing an effective initial benefit statement.

**SDR Accounts**
* The SDR Team Lead will assign 25-30 accounts per quarter in Salesforce from the Strategic and Large segments to a SDR. These accounts will be provided in close partnership with a Regional Sales Director. The accounts will be identified and selected as the best accounts to focus on in order to meet our revenue objectives.
* The accounts will be a mixture of Existing Customer accounts with small GitLab Licenses, Community Edition Accounts, and Accounts that are currently not using GitLab  

**Account Research**
* Now that you have a book of business, the next step is determining which accounts and roles at those companies you should approach first. This can be determined in conjunction with your assigned account executive or SDR Manager. Chances are they may have prospected into the account in the past or have some telling information about the account. Weekly syncs on assigned accounts will directly impact your success as an SDR. To be prepared for that weekly sync and your everyday prospecting it helps to do the following:
 * Create a targeted list; look for key indicators like number of existing contacts, historical opportunities, CE usage, and EE customers
 * Within that list, identify the buyers based on GitLab's buyer personas, keep the list short and simple for each account
 * Update your chosen target accounts in Salesforce by looking for any missing data points such as phone numbers, emails, linkedin profiles, contacts etc.
 * Look for contacts with IT leadership and application development leadership responsibilities. This can be senior technical individual contributors such as software architects, or leaders in the organization's IT department all the way up to a CIO or CTO.
 * Filter in, filter out. Alternate your target accounts from your main 30 accounts to keep you going.

Leveraging the data gathered above, a SDR should spend no more than three minutes performing account research to personalize messaging. Below is an outline to help you come up with a personalized message in three minutes or less

**Personalized message research help and example:**

Three things in three minutes about the Organization, Industry, Prospect

* **Salesforce**
 * Locate the account in Salesforce
 * Are there other leads that are being worked?
 * Is this a customer account?
 * Are there any open or closed opportunities? Are we able to reference these? 
 * Activity history
 * Who in the organization have we been in contact with? Can we follow up?
* **Linkedin**
 * Refer to the prospect's summary. Is there any mutual interest you share? 
 * Does anything stand out that is relevant to their needs as an organization? 
 * What is the prospect's role and how does that affect your messaging?
 * Have they published any articles that would be work referencing?
 * Work history, any GitLab customers that we can reference?
 * Personalization: Any mutual Hobbies/Interests you share? Location? Education?
 * Connections, are they connected to anyone at GitLab for a possible introduction? If so, please slack that individual to see if they would be willing to make a introduction
* **Company Website**
 * Mission statement
 * Press releases/Newsroom
 * Major IT Initiatives 
 * (ctrl F) search for keywords

**Prospecting - The Outreach**
* At GitLab, the top performing SDRs consistently make and send out a high volume of phone calls and personalized emails in their prospecting efforts. 

**Cold Calling**

With all the technology availiable to you as a SDR here at GitLab there really isn't such a thing as a "cold" call. Below are a few important tips that will help you structure a prospecting call:
* Call about them, not you. Don't be what everyone thinks a salesperson is, don't throw up on them with cool information about GitLab
* Always have their LinkedIn profile up 
* Listen. Provide value for them to continue conversations with GitLab 
* Ensure that you are calling at the right times. Data suggests in the morning, at lunch, and in the evening as best call practices. Schedule your day accordingly. 
* Be confident and passionate
* Focus on decision makers, but don't leave the other roles hanging
* Practice with other SDRs or your Team Lead on objection handling
* Obtain a commitment

**Prospecting Call Structure**
* Introduction
    * Immediately introduce yourself and GitLab
    * Ask for time
* Initial benefit statement
* Qualification, ask questions, listen
* Obtain the commitment, ask for the meeting, come prepared with a few times off hand that work for you and be specific

**Dealing with rejection over the phone**
* You will be told "no" a lot. Don't let this bother you, ask yourself after every call; "What could I have done better or differently?" Then gather yourself and approach the next call.
* Do we end the call when we hear the first objection? NO! If you have someone on the phone already don't lose the chance to have a conversation. Below is a guide for navigating objections in prospecting calls called the *3 No Rule*

**3 No Rule**

**A- Approach/Intro**
* Hi (prospect's name), this is (your name) calling with GitLab
    * ...I know you weren't expecting my call... so I will be brief OR ...do you have a few minutes?
    * ...Did I catch you at an okay time?
    * ...have you hear of GitLab?

**B- Brief Initial Benefit Statement (The Why)**
* I recently (read/saw/spoke to/understand) that (something you found in your pre call research)...
    * The purpose of my call is...
    * ...While I have you on the phone, I had a couple of quick questions...

**C- Connect/Ask Questions**
* Have you heard of GitLab?
    * "No" - Tell them
    * "Yes"
        * Where did you hear about it?
        * Have you used it before?
    * What are you using for... (topic of your initial benefit statement)?
    * What do you like about X tool?
    * Anything you would change about X tool if you could?

**D- Why Now?**
* After you have found an area to add value to:
    * Give initial benefit statement (focusing on how we can help them, give examples, share case studies)
    * Recommend next steps based on identified need, pain, and goals
    * Be confident, assumptive, and consultative

**E- Obtaining the commitment**
* Meeting first - If they object, bring up the need, pain and goals that you have identified, reposition yourself and ask again if they could get X value, would it be worth a 5-10 minute call?
* Why not?
    * if still a "no" suggest they attend or watch a recorded webinar on our product
        * If still a "no", recommend a case study or article. Anything that you can follow up with them on

**F- Follow up**
* Send a recap email with what you told them you would send
* Record the notes about your conversation in Salesforce and set tasks for future follow up

**The Handoff**

You were successful and created an opportunity! Now what? The handoff can make or break an opportunity being accepted by sales. This simple but important step in passing the prospect to sales requires the following:
* Send out a meeting invite to both the prospect and SAL
* [Create the Opportunity](https://about.gitlab.com/handbook/sales-process/images_sales_process/#when-to-create-an-opportunity-) with the correct fields
* After the initial meeting date, sales will accept the opportunity and take it from there

## Resources
* [Business Operations Handbook](https://about.gitlab.com/handbook/business-ops/)
* [Marketing Handbook](https://about.gitlab.com/handbook/marketing/)
* [Sales Handbook](https://about.gitlab.com/handbook/sales/)
* [GitLab Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq/)
* [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
* [Sales Qualification Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
* [GitLab Sales Demo](https://about.gitlab.com/handbook/sales/demo/)
* [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
* [FAQ from Prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
* [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
* [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Common Terms Glossary](https://about.gitlab.com/handbook/business-ops/#glossary)

## Asking Questions

* Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to ge your work done faster with your team than being held up at a road block.
* [Who to Talk to for What](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)

## Referring people to the SDR team

Know someone who works at a company with at least 750 technical employees? Please introduce them to a Sales Development Representative Lead. When an SDR receives an introduction, they will respectfully ask the person that was introduced to them if they would be willing to point them to any people who are responsible for evaluating or selecting SCM, CI/CD, application monitoring, and/or software project management solutions. This approach helps ensure we're talking to the right people, and helps reduce the time it takes to identify the people within a company who would be best suited to hear about how GitLab can help their organization ship better software, faster.

You can use the following text to make an introduction over email:

```
Hi {contact.first.name},

My colleague {sdr.lead.first.name} has been trying to find people responsible for evaluating and selecting SCM and CI/CD solutions at {contact.organization}. Could you do me a favor and point them in the right direction? They would be asking to schedule a quick call to discuss the possibility of using GitLab at {contact.organization}. Thank you, I appreciate your help.

Best,

{first.name}
```