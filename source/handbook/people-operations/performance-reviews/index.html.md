---
layout: markdown_page
title: "Performance Reviews"
---
## On this page
{:.no_toc}

- TOC
{:toc}


## Performance Review Changes in 2018

The People Ops team is looking to add 360 reviews to include:
  * manager to direct report
  * direct report to manager
  * peer to peer in the reviews.
  * migrate away from using Lattice

### Performance Review Process

**Q: What will performance reviews be based on?**

**A:** Reviews will include six questions related to our [values](/handbook/values/) which will be rated by both the individual and their manager. There will also be two questions focused on recognizing strengths and development opportunities.

Individual Contributor (IC) and Manager Self Reviews include the following questions:
1. How well did this person collaborate with people inside and outside the team?
1. What results did this person get in achieving the team's OKRs?
1. How well has this person been able to achieve boring solutions while maintaining velocity?
1. How has this person worked to support, promote, and/or embody diversity in their role?
1. How well did this person iterate on each project?
1. How well did this person maintain transparency according to the GitLab workflow?
1. In what areas did you demonstrate the most growth in the first half of 2017?
1. Looking forward, in what areas would you most like to develop your knowledge, skills and abilities?

**Q: How long should the response to each question be?**

**A:** Comments should be to the point and include specific examples. Each performance review should be unique, therefore People Ops cannot link an example of a great manager to direct report performance review, as it could be used as a template to copy and paste. Instead we encourage managers to reach out to People Ops to discuss any questions or work through the performance cycle.

**Managers:**
In cases where you’ve identified your top performer, we should learn from what makes that person successful to share with others.
In cases where below average performance is identified, you should plan to deliver a PIP to clearly identify performance gaps and expected changes.

### Performance Review Feedback Meeting

* No surprises. Team members should never hear about positive or performance in need of improvement for the first time at the performance review meeting. Team members should have regular [1:1s](https://about.gitlab.com/handbook/leadership/1-1/) where this is discussed.
* The overall aim of a performance review is to shine light on and discuss critical points (in writing), followed by a conversation between the team member and their manager. Don't allow the performance review (document and conversation) to (d)evolve into a "todo" list.
* Share the form with the team member in advance of the meeting so they can prepare and come to the meeting with questions and discussion points
* Make sure you (Manager) are also prepared for the discussion, write down some notes and key points you want to make
* Make time to talk about the future - [career development](https://about.gitlab.com/handbook/leadership/1-1/#career-development-discussion-at-the-1-1), professional growth etc
* If there are areas that need improvement, plan to deliver a [PIP](https://about.gitlab.com/handbook/underperformance/)
* This should be a conversation, try to avoid doing all the talking and get feedback from the team member. Ask questions such as:
   1. How can I support you with progressing to the next experience level for your position?
   1. How can I be a better manager for you?
   1. What are you hoping to achieve at GitLab this coming year?
* Avoid the [horns and halo effects of recent events](https://www.thebalance.com/effective-performance-review-tips-1918842), and instead make sure to take a step back to review the entire review period.
* Make sure you discuss _positive_ aspects of performance, but avoid using the ["feedback sandwich"](https://www.officevibe.com/blog/employee-feedback-examples) to mask an honest conversation about areas that need improvement
* Follow Up. Did you discuss pathways to career progress, or specific points of attention for improving performance? Make sure you add them to the top of the 1:1 doc so as to remind yourselves to follow up every so often.
