require 'date'
require 'yaml'

# Don't generate for versions prior to 8.0
CUTOFF = Date.new(2015, 9, 22)

# Generates the Markdown used by the `/releases/` page based on monthly
# release blog posts in this repository
class ReleaseList
  def generate(output = StringIO.new)
    release_posts.each do |post|
      output.puts "## [GitLab #{post.version}](#{post.relative_url})"
      output.puts post.date.to_time.strftime('%A, %B %e, %Y').to_s
      output.puts '{: .date}'
      output.puts

      post.highlights.each do |highlight|
        output.puts "- #{highlight}"
      end

      output.puts
    end

    # Return the final string if `output` supports it
    output.string if output.respond_to?(:string)
  end

  # Returns an Array of monthly release posts in descending order
  def release_posts
    root = File.expand_path('../source/posts', __dir__)

    # find's `-regex` option is too dumb to do what we want, so use grep too
    find = %(find "#{root}" -type f -iname "*-released.html.md")
    grep = %(grep #{grep_flags} '\\d{4}-\\d{2}-22-gitlab-\\d{1,2}-\\d{1,2}-released')
    sort = %q(sort -n)

    `#{find} | #{grep} | #{sort}`
      .lines
      .map    { |path| ReleasePost.new(path) }
      .reject { |post| post.date < CUTOFF }
      .reverse
  end

  private

  def grep_flags
    # GNU supports PCRE via `-P`; for others (i.e., BSD), we want `-E`
    if `grep -V`.include?('GNU grep')
      '-P'
    else
      '-E'
    end
  end

  class ReleasePost
    attr_reader :filename, :title, :date, :version

    def initialize(filename)
      @filename = filename.strip

      extract_attributes
    end

    def relative_url
      format('/%d/%0.2d/%0.2d/%s', date.year, date.month, date.day, title)
    end

    # Returns an Array of "highlights"
    #
    # If a data file exists for the release post, we extract the feature list
    # from its YAML. If not, we read the entire release post and consider
    # anything with a level two Markdown header - `##` - a highlighted feature.
    #
    # Finally, the list is filtered to exclude anything in the `EXCLUSIONS`
    # list.
    def highlights
      return @highlights if @highlights

      @highlights =
        if data_file?
          highlights_from_data_file
        else
          highlights_from_post
        end

      @highlights
        .reject! { |l| EXCLUSIONS.any? { |e| l.downcase.start_with?(e) } }

      @highlights
    end

    private

    # These headers are in every post and should not be considered highlights
    EXCLUSIONS = [
      'other changes',
      'upgrade barometer',
      'installation',
      'updating',
      'enterprise edition',
      'deprecations',
      'other improvements',
      "this month's most valuable person"
    ].freeze

    def extract_attributes
      match = filename.match(
        /
          (?<date>\d{4}-\d{2}-\d{2})
          -
          (?<title>
            gitlab-
            (?<major>\d{1,2})-(?<minor>\d{1,2})
            -released
          )
        /xi
      )

      @title   = match['title']
      @date    = Date.parse(match['date'])
      @version = "#{match['major']}.#{match['minor']}"
    end

    def data_file?
      File.exist?(data_file_path)
    end

    def data_file_path
      filename = File.basename(@filename, '.html.md').tr('-', '_')
      File.expand_path("../data/release_posts/#{filename}.yml", __dir__)
    end

    def highlights_from_post
      File
        .read(filename)
        .lines
        .select { |l| l =~ /\A##[^#]/ }
        .map    { |l| l.sub(/\A##([^#]+)\z/, '\1').strip }
        .map    { |l| remove_editions(l) }
    end

    def highlights_from_data_file
      features = YAML.safe_load(File.read(data_file_path)).fetch('features', {})

      features
        .values
        .flatten
        .collect { |f| f['name'] }
    end

    def remove_editions(line)
      line
        .sub(/ eep\z/, '')
        .sub(/ ees\z/, '')
        .sub(/ ce\z/, '')
    end
  end
end

# Print to stdout when this file is run directly
ReleaseList.new.generate($stdout) if $PROGRAM_NAME == __FILE__
